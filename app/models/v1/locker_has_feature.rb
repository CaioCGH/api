class V1::LockerHasFeature < ApplicationRecord
  belongs_to :v1_locker, optional: true
  belongs_to :v1_feature, optional: true
end
