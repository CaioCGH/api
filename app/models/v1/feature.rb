class V1::Feature < ApplicationRecord
  belongs_to :v1_position, optional: true
  belongs_to :v1_place, optional: true
end
