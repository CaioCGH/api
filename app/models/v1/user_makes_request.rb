class V1::UserMakesRequest < ApplicationRecord
  belongs_to :v1_user, optional: true
  belongs_to :v1_request, optional: true
end
