Rails.application.routes.draw do
  namespace :v1 do
    resources :requests
  end
  namespace :v1 do
    resources :features
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
