# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

V1::Status.create!([{ status: 'registered' },
                    { status: 'reserved' },
                    { status: 'quitter' },
                    { status: 'renter' }])

V1::UserRole.create!([{ user_role: 'student' },
                      { user_role: 'entity' }])

V1::Place.create!([{ place: 'inside' },
                   { place: 'outside' }])

V1::Position.create!([{ position: 'high' },
                      { position: 'low' }])

V1::ContractType.create!([{ contract_type: 'semiannual' },
                          { contract_type: 'annual' }])

# Fill all features
V1::Place.all.to_a.product(V1::Position.all.to_a).each do | place, position |
  V1::Feature.create!({ v1_place_id: place.id, v1_position_id: position.id })
end

case Rails.env
when "development"
  # We shouldn't have a locker available that is deactivated
  u02 = V1::Locker.create!({ number: 'U02', available: false, deactivated: true })
  u03 = V1::Locker.create!({ number: 'U03', available: false, deactivated: false })
  u10 = V1::Locker.create!({ number: 'U10', available: true, deactivated: false })

  V1::LockerHasFeature.create!({ v1_locker_id: u02.id, v1_feature_id: 1 })
  V1::LockerHasFeature.create!({ v1_locker_id: u03.id, v1_feature_id: 1 })
  V1::LockerHasFeature.create!({ v1_locker_id: u10.id, v1_feature_id: 2 })

  jao = V1::User.create!({ name: 'jao',
                           email: 'jao@usp.br',
                           nusp: 42,
                           v1_user_roles_id: 1 })

  jaos_request = V1::Request.create!({ renew: false,
                                       v1_contract_type_id: jao.id })

  V1::UserMakesRequest.create!({ v1_user_id: jao.id,
                                 v1_request_id: jaos_request })

  V1::RequestSuggestsFeature.create!([
    { v1_request_id: jaos_request.id, v1_feature_id: 1 },
    { v1_request_id: jaos_request.id, v1_feature_id: 2 },
  ])

  V1::UserReservesLocker.create!({ v1_user_id: jao.id,
                                   v1_locker_id: u03.id,
                                   start_date: Date.new(2017, 12, 15),
                                   end_date: Date.new(2018, 01, 27),
                                   paid: true })

  V1::UserOwnsLocker.create!({ v1_user_id: 1,
                               v1_locker_id: 2,
                               start_date: Date.new(2018, 01, 28),
                               end_date: Date.new(2018, 07, 31) })
end
