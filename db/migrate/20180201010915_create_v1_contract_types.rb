class CreateV1ContractTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_contract_types do |t|
      t.string :contract_type

      t.timestamps
    end
    add_index :v1_contract_types, :contract_type, unique: true
  end
end
