class CreateV1Requests < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_requests do |t|
      t.boolean :renew
      t.references :v1_contract_type, foreign_key: true

      t.timestamps
    end
  end
end
